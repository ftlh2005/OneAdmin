$.ajaxSetup(
{
    cache : false
}
);
(function ($)
{
    //导航菜单
    $.fn.duxMenu = function (options)
    {
        var defaults =
        {
            json : {},
            sidebar : '#sidebar',
            menu : '#menu',
            bread : '#bread',
            scrollbar : true
        }
        var options = $.extend(defaults, options);
        this.each(function ()
        {
            var nav = $(this);
            //主导航
            var navHtml = '';
            for (var i in options.json)
            {
                navHtml += '<li><a href="javascript:;" data="' + i + '">' + options.json[i].name + '</a></li>';
            }
            nav.html(navHtml);
            if (options.scrollbar)
            {
                var menuScrollbar = $(options.menu).jScrollPane();
                var menuApi = menuScrollbar.data('jsp');
                $(window).bind('resize', function ()
                {
                    menuApi.reinitialise();
                }
                );
            }
            //菜单导航
            nav.find("a").click(function ()
            {
                var label = $(this).attr('data');
                var list = options.json[label]['menu'];
                menuShow(list, label);
                $(options.menu).find('li a:first').click();
            }
            );
            //菜单显示
            function menuShow(list, label)
            {
                var html = '';
                for (var i in list)
                {
                    if ($.isArray(list[i].name))
                    {
                        list[i].name = list[i].name[0];
                    }
                    html += '<h2>' + list[i].name + '</h2><ul>';
                    var menu = list[i].menu;
                    for (var o in menu)
                    {
                        if (menu == '' || menu == null)
                        {
                            continue;
                        }
                        html += '<li><a href="javascript:;" data="' + menu[o].url + '" top="' + label + '">';
                        if (menu[o].ico != null)
                        {
                            html += '<i class="u-icon">' + menu[o].ico + '</i> ';
                        }
                        if ($.isArray(menu[o].name))
                        {
                            menu[o].name = menu[o].name[0];
                        }
                        html += '<span>' + menu[o].name + '</span></a></li>';
                    }
                    html += '</ul>';
                }
                if (options.scrollbar)
                {
                    menuApi.getContentPane().html(html);
                    menuApi.reinitialise();
                }
                else
                {
                    $(options.menu).html(html);
                }
            }
            //菜单点击
            $(options.menu).on('click', 'a', function ()
            {
                $(options.menu).find('a').removeClass("current");
                $(this).addClass('current');
                $('iframe').attr('src', $(this).attr('data'));
            }
            );
            
            //初次刷新
            nav.find("a:first").click();
        }
        );
    };
    //表格处理
    $.fn.duxTable = function (options)
    {
        var defaults =
        {
            selectAll : '#selectAll',
            deleteUrl : ''
        }
        var options = $.extend(defaults, options);
        this.each(function ()
        {
            var table = this;
            //处理多选单选
			
            $(options.selectAll).click(function ()
            {
				if (!!$(options.selectAll).attr("checked"))
                {
                    $(table).find("[name='id[]']").each(function ()
                    {
                        $(this).attr("checked", 'true');
                    }
                    )
                }
                else
                {
                    $(table).find("[name='id[]']").each(function ()
                    {
                        $(this).removeAttr("checked");
                    }
                    )
                }
            }
            );
            //处理删除
            $(table).find('.u-del').click(function ()
            {
                var obj = this;
                var div = $(obj).parent().parent();
                var url = '';
                if (options.deleteUrl == '')
                {
                    url = $(obj).attr('url');
                }
                else
                {
                    url = options.deleteUrl;
                }
                $.dialog.confirm('你确认删除操作？', function ()
                {
                    $.post(url,
                    {
                        data : $(obj).attr('data')
                    }, function (json)
                    {
                        if (json.status == 1)
                        {
                            div.remove();
                            $.dialog.tips(json.info, 1);
                        }
                        else
                        {
                            $.dialog.tips(json.info, 3);
                        }
                    }, 'json');
                }, function ()
                {
                    return;
                }
                );
            }
            );
            //处理删除
            $(table).find('.u-del-id').click(function ()
            {
                var obj = this;
                var div = $(obj).parent().parent();
                var url = '';
                if (options.deleteUrl == '')
                {
                    url = $(obj).attr('url');
                }
                else
                {
                    url = options.deleteUrl;
                }
                $.dialog.confirm('你确认删除操作？', function ()
                {
                    $.post(url,
                    {
                        id : $(obj).attr('data')
                    }, function (json)
                    {
                        if (json.status == 1)
                        {
                            div.remove();
                            $.dialog.tips(json.info, 1);
                        }
                        else
                        {
                            $.dialog.tips(json.info, 3);
                        }
                    }, 'json');
                }, function ()
                {
                    return;
                }
                );
            }
            );
            //处理ajax编辑
            $(table).find('.u-edit').click(function ()
            {
                var oldText = $(this).text();
                var width = $(this).attr('width');
                var obj = this;
                var input = $(obj).find('input');
                var url = $(obj).attr('url');
                var name = $(obj).attr('name');
                if (input.length == 0)
                {
                    var html = '<input class="u-ipt" style="width:' + width + 'px;" value="' + oldText + '" type="text" />';
                    $(obj).html(html);
					input = $(obj).find('input');
                }
				input.focus();
                input.blur(function ()
                {
                    text = input.val();
                    $.post(url,
                    {
                        name : name,
                        data : text
                    }, function (json)
                    {
                        if (json.status == 1)
                        {
                            $(obj).text(json.info);
                        }
                        else
                        {
                            input.addClass('u-tta-err');
                            $.dialog.tips(json.info);
                        }
                    }, 'json');
                }
                );
            }
            );
        }
        );
    };
    //表单处理
    $.fn.duxForm = function (options)
    {
        var defaults =
        {
            status : '#status',
            postFun : {},
            returnUrl : '',
			returnFun : {}
        }
        var options = $.extend(defaults, options);
        this.each(function ()
        {
            var form = this;
            //添加关联KEY
            var key = new Date();
            key = key.getTime();
            if ($("#relation_key").length > 0)
            {
                if ($("#relation_key").val() == null)
                {
                    $("#relation_key").val(key);
                }
            }
            else
            {
                $(form).append('<input name="relation_key" id="relation_key" type="hidden" value="' + key + '" />');
            }
            //表单处理
            $(form).Validform(
            {
                ajaxPost : true,
                postonce : true,
                tiptype : function (msg, o, cssctl)
                {
                    if (!o.obj.is("form"))
                    {
                        var objtip = o.obj.siblings("p");
                        cssctl(objtip, o.type);
                        objtip.text(msg);
                        if(o.type == 3){
                            $(options.status).addClass('status-err');
                            $(options.status).text('您要填写的信息不完全，请检查后提交！');
                        }else{
                            $(options.status).removeClass('status-err');
                            $(options.status).text('');
                        }
                    }
                    else
                    {
                        $(options.status).text(msg);
                    }
                },
                beforeSubmit:function(curform){
                    if($.isFunction(options.postFun)){
                        options.postFun();
                    }
                },
                callback : function (data)
                {
                    $(options.status).click();
                    $(options.status).removeClass('status-err');
                    if (data.status == 1)
                    {
						if($.isFunction(options.returnFun)){
							options.returnFun(data);
						}else{
							if (options.returnUrl == null || options.returnUrl == '')
							{
								$(options.status).html(data.info + '马上为您重新载入！');
								window.setTimeout(function ()
								{
									window.location.reload();
								}, 3000);
							}
							else
							{
								var html = ' 您可以 <a href="javascript:window.location.reload(); ">刷新</a> 或 <a href="' + options.returnUrl + '">返回</a>，无操作5秒后会返回。';
								$(options.status).html(data.info + html);
								window.setTimeout(function ()
								{
									window.location.href = options.returnUrl;
								}, 5000);
							}
						}
                    }
                    else
                    {
                        $(options.status).addClass('status-err');
                        $(options.status).html(data.info);
                    }
                }
            }
            );
            
        }
        );
    };
    
    //编辑器调用
    $.fn.duxEditor = function (options)
    {
        var defaults =
        {
            uploadUrl : rootUrl + 'index.php?r=admin/Upload/index',
            fileUrl : '',
            uploadParams :
            {
                relation_key : $('#relation_key').val()
            },
            config : {}
        }
        var options = $.extend(defaults, options);
        this.each(function ()
        {
            //编辑器
            var id = this;
            var editorConfig =
            {
                allowFileManager : false,
                uploadJson : options.uploadUrl,
                fileManagerJson : options.fileUrl,
                extraFileUploadParams : options.uploadParams,
                afterBlur : function ()
                {
                    this.sync();
                }
            };
            editorConfig = $.extend(editorConfig, options.config);
            var editor = KindEditor.create(id, editorConfig);
        }
        );
    };
    
    //颜色
    $.fn.duxColor = function (options)
    {
        var defaults = {}
        var options = $.extend(defaults, options);
        this.each(function ()
        {
            $(this).soColorPacker();
        }
        );
    };
    
    //上传调用
    $.fn.duxFileUpload = function (options)
    {
        var defaults =
        {
            uploadUrl : rootUrl + 'index.php?r=admin/Upload/index',
            uploadParams :
            {
				'session_id' : sessionId,
                relation_key : $('#relation_key').val()
            },
            complete : function ()  {},
            uploadParamsCallback : function ()  {},
            type : ''
        }
        var options = $.extend(defaults, options);
        this.each(function ()
        {
            var upButton = $(this);
            var urlVal = upButton.attr('data');
            urlVal = $('#' + urlVal);
            var buttonText = upButton.text();
            var preview = upButton.attr('preview');
            preview = $('#' + preview);
            /* 图片预览 */
            preview.click(function ()
            {
                if (urlVal.val() == '')
                {
                    $.dialog.tips('没有发现已上传图片！', 2);
                }
                else
                {
                    window.open(urlVal.val());
                }
                return;
            }
            );
			/*创建上传*/
			upButton.uploadify({
				'formData' : {},
				'method'   : 'post',
				'swf'      : baseDir + '/upload/uploadify.swf',
				'uploader' : options.uploadUrl,
				'auto' : true,
				'height' : '100%',
				'width' : '100%',
				'buttonText' : '上传',
				'multi' : false,
				'fileTypeDesc' : '指定类型文件',
				'fileTypeExts' : options.type,
				'onSelectError' : uploadify_onSelectError,
    			'onUploadError' : uploadify_onUploadError,
				'onFallback' : function() {
					$.dialog.tips('未检测到兼容版本的Flash，请先安装flash控件！', 2);
				},
				'onUploadProgress':function(file,bytesUploaded,bytesTotal,totalBytesUploaded,totalBytesTotal){
					var num = Math.round(bytesUploaded / bytesTotal * 10000) / 100.00 + "%";
					upButton.uploadify('settings','buttonText','上传：' + num);
				},
				'onUploadStart' : function(file) {
					upButton.uploadify('disable', true);
					var ParamsCallback = options.uploadParamsCallback();
                	var Params = $.extend(options.uploadParams, ParamsCallback);
					upButton.uploadify('settings','formData',Params);
				},
				'onUploadSuccess' : function(file, data, response) {
					data = eval('(' + data + ')');
					if (data.error)
					{
						$.dialog.tips(data.message, 2);
						return false;
					}
					else
					{
						urlVal.val(data.url);
						options.complete(data.info, urlVal);
					}
					
				},
				'onQueueComplete' : function(queueData) {
					upButton.uploadify('disable', false);
					upButton.uploadify('settings','buttonText','上传');
					upButton.uploadify('cancel','*');
				},
				'overrideEvents' : [ 'onDialogClose', 'onUploadError', 'onSelectError' ]
			});
        }
        );
    };
    
    //多图上传
    $.fn.duxMultiUpload = function (options)
    {
        var defaults =
        {
            uploadUrl : rootUrl + 'index.php?r=admin/Upload/index',
            fileList : rootUrl + 'index.php?r=admin/Attachment/fileList',
            uploadParams :
            {
                relation_key : $('#relation_key').val()
            },
            complete : function ()  {},
            uploadParamsCallback : function ()  {},
            type : '',
            UploadMaxNum : 20
        }
        var options = $.extend(defaults, options);
        this.each(function ()
        {
            var upButton = $(this);
            var dataName = upButton.attr('data');
            var div = $('#' + dataName);
            var data = div.attr('data');
            var buttonText = upButton.text();
            //获取列表
			dataNum = div.find('li').length;
            if (data != null && dataNum == 0)
            {
				div.html('');
                $.post(options.fileList,
                {
                    id : data
                }, function (json)
                {
                    if (json.status == 1)
                    {
                        for (var i in json.info)
                        {
                            htmlList(json.info[i].info);
                        };
                        if (json.info[1].info != null)
                        {
                            div.sortable().on('sortupdate', function ()  {}
                            
                            );
                        }
                    }
                }, 'json');
            }
			/*创建上传*/
			upButton.uploadify({
				'formData' : {},
				'method'   : 'post',
				'swf'      : baseDir + '/upload/uploadify.swf',
				'uploader' : options.uploadUrl,
				'auto' : true,
				'height' : '100%',
				'width' : '100%',
				'queueSizeLimit' : options.UploadMaxNum,
				'buttonText' : '上传',
				'fileTypeDesc' : '指定类型文件',
				'fileTypeExts' : options.type,
				'onSelectError' : uploadify_onSelectError,
    			'onUploadError' : uploadify_onUploadError,
				'onFallback' : function() {
					$.dialog.tips('未检测到兼容版本的Flash，请先安装flash控件！', 2);
				},
				'onInit'   : function(instance) {
				},
				'onUploadProgress':function(file,bytesUploaded,bytesTotal,totalBytesUploaded,totalBytesTotal){
					var num = Math.round(bytesUploaded / bytesTotal * 10000) / 100.00 + "%";
					upButton.uploadify('settings','buttonText','上传：' + num);
				},
				'onUploadStart' : function(file) {
					upButton.uploadify('disable', true);
					var ParamsCallback = options.uploadParamsCallback();
                	var Params = $.extend(options.uploadParams, ParamsCallback);
					upButton.uploadify('settings','formData',Params);
				},
				'onUploadSuccess' : function(file, data, response) {
					data = eval('(' + data + ')');
					if (data.error)
					{
						$.dialog.tips(data.message, 2);
						upButton.uploadify('cancel','*');
						return false;
					}
					else
					{
						htmlList(data.info);
                    	options.complete(data.info, file);
					}
					
				},
				'onQueueComplete' : function(queueData) {
					upButton.uploadify('disable', false);
					upButton.uploadify('settings','buttonText','上传');
					upButton.uploadify('cancel','*');
					div.sortable().on('sortupdate', function ()  {});
				},
				'onSelect' : function(file) {
					num = div.find('li').length;
					debug (file);
					if (num >= options.UploadMaxNum)
					{
						$.dialog.tips('最多只能上传' + options.UploadMaxNum + '张图片', 2);
						upButton.uploadify('cancel',file.id);
					}
				},
				'overrideEvents' : [ 'onDialogClose', 'onUploadError', 'onSelectError' ]
			});
			
            //处理上传列表
            function htmlList(file)
            {
                var html = '<li>\
                                        					<a class="close" href="javascript:;" onclick="$(this).parent().remove();">×</a>\
                                        					<div class="img"><span class="pic"><img src="' + file.url + '" width="80" height="80" /></span></div>\
                                        					<div class="title">\
                                        					<input name="' + dataName + '[id][]" type="hidden" value="' + file.file_id + '" />\
                                        					<input name="' + dataName + '[title][]" type="text" value="' + file.title + '" />\
                                        					</div>\
                                        				</li>';
                options.complete(file, upButton);
                div.append(html);
            }
            //处理删除
            div.on('click', '.close', function ()
            {
                $(this).parent().remove();
            }
            );
            
        }
        );
    };
    //表单页面处理
    $.fn.duxFormPage = function (options)
    {
        var defaults =
        {
            uploadUrl : rootUrl + 'index.php?r=admin/Upload/index',
            fileList : rootUrl + 'index.php?r=admin/Attachment/fileList',
            uploadComplete : function ()  {},
            uploadParamsCallback : function ()  {},
            UploadMaxNum : 20,
            postFun : {},
            returnUrl : '',
			returnFun : {}
        }
        var options = $.extend(defaults, options);
        this.each(function ()
        {
            var form = this;
            form = $(form);
            //表单处理
            form.duxForm(
            {
                status : '#status',
                postFun : options.postFun,
                returnUrl : options.returnUrl,
				returnFun : options.returnFun
            }
            );
            //多图片上传
            if ($(".u-multi-upload").length > 0)
            {
                form.find('.u-multi-upload').duxMultiUpload(
                {
                    type : '*.jpg; *.gif; *.png; *.bmp; *.jpe',
                    uploadUrl : options.uploadUrl,
                    fileList : options.fileList,
                    complete : options.uploadComplete,
                    uploadParamsCallback : options.uploadParamsCallback,
                    UploadMaxNum : options.UploadMaxNum
                }
                );
            }
            //图片上传
            if ($(".u-img-upload").length > 0)
            {
                form.find('.u-img-upload').duxFileUpload(
                {
                    type : '*.jpg; *.gif; *.png; *.bmp; *.jpeg',
                    uploadUrl : options.uploadUrl,
                    complete : options.uploadComplete,
                    uploadParamsCallback : options.uploadParamsCallback
                }
                );
            }
            //文件上传
            if ($(".u-file-upload").length > 0)
            {
                form.find('.u-file-upload').duxFileUpload(
                {
                    type : '*.*',
                    uploadUrl : options.uploadUrl,
                    complete : options.uploadComplete,
                    uploadParamsCallback : options.uploadParamsCallback
                }
                );
            }
            //编辑器
            if ($(".u-editor").length > 0)
            {
                form.find('.u-editor').duxEditor(
				{
					uploadUrl : options.uploadUrl
				});
            }
            //颜色
            if ($(".u-color").length > 0)
            {
                form.find('.u-color').duxColor();
            }
            //时间选择
            if ($(".u-time").length > 0)
            {
                form.find('.u-time').calendar(
                {
                    format : 'yyyy-MM-dd HH:mm:ss'
                }
                );
            }
            //联动菜单
            if ($(".u-linkage").length > 0)
            {
                form.find('.u-linkage').duxLinkMenu();
            }
            //TAB菜单
            if ($(".u-tabs").length > 0)
            {
                $(".u-tabs li .tab-a").powerSwitch(
                {
                    classAdd : "tab-on"
                }
                );
            }
			//地区库
            if ($(".u-area").length > 0)
            { 
				form.find('.u-area').duxArea({
					url : rootUrl + 'index.php?r=duxcms/Region/json'
				});
            }
        }
        );
    };
    
    //AJAX操作带确认
    $.fn.duxAjaxConfirm = function (options)
    {
        var defaults =
        {
            url : '',
            content : '',
            params : function ()  {},
            complete : function ()  {},
            failure : function ()  {}
        }
        var options = $.extend(defaults, options);
        this.each(function ()
        {
            var obj = this;
            $(obj).click(function ()
            {
                var params = options.params(obj);
                $.dialog.confirm(options.content, function ()
                {
                    $.post(options.url, params, function (json)
                    {
                        if (json.status == 1)
                        {
                            options.complete(json.info, obj);
                            $.dialog.tips(json.info, 2);
                        }
                        else
                        {
                            options.failure(json.info, obj);
                            $.dialog.tips(json.info, 2);
                        }
                    }, 'json');
                    
                }
                );
            }
            );
            
        }
        );
    };

    /* 设置表单的值 */
    setValue = function(name, value){
        var first = name.substr(0,1), input, i = 0, val;
        if(value === "") return;
        if("#" === first || "." === first){
            input = $(name);
        } else {
            input = $("[name='" + name + "']");
        }

        if(input.eq(0).is(":radio")) { //单选按钮
            input.filter("[value='" + value + "']").each(function(){this.checked = true});
        } else if(input.eq(0).is(":checkbox")) { //复选框
            if(!$.isArray(value)){
                val = new Array();
                val[0] = value;
            } else {
                val = value;
            }
            for(i = 0, len = val.length; i < len; i++){
                input.filter("[value='" + val[i] + "']").each(function(){this.checked = true});
            }
        } else {  //其他表单选项直接设置值
            input.val(value);
        }
    }
    //ajax get请求
    $('.ajax-get').click(function(){
        var target;
        var that = this;
        var tips = $(that).text();
        if ((target = $(this).attr('url'))) {
            $.dialog.confirm('确定要'+tips+'吗?', function () {
                $.get(target, function (json) {
                    if (json.status == 1) {
                        $.dialog.tips(json.info, 2);
                        window.setTimeout(function ()
                        {
                            window.location.reload();
                        }, 1500);
                    }else {
                        $.dialog.tips(json.info, 2);
                    };
                });
            }, function () {
                return;
            });
        };
    });


}
)(jQuery);
