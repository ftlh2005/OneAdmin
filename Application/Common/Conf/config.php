<?php
// +----------------------------------------------------------------------
// |  公共配置文件
// +----------------------------------------------------------------------
// | Copyright (c) 2003-2014 http://www.coolhots.net/ All rights reserved.
// +----------------------------------------------------------------------
// | Author: CoolHots <coolhots@163.com>
// +----------------------------------------------------------------------
// | Date: 2014-5-22
// +----------------------------------------------------------------------

$db = require ('db.php');
$version = require ('version.php');
$d = array_merge($b, $a);
$config= array (

		/**
		 * 模块相关配置
		 */
		/**
		 * 扩展模块列表
		 */
		'AUTOLOAD_NAMESPACE' => array('Addons' => ONEDREAM_ADDON_PATH), //扩展模块列表
		
		// 是否开启令牌验证 默认关闭
		'TOKEN_ON'	=> false,
		// 令牌验证的表单隐藏字段名称，默认为__hash__
		'TOKEN_NAME' => '__hash__',
		//令牌哈希验证规则 默认为MD5
		'TOKEN_TYPE' => 'md5',
		//令牌验证出错后是否重置令牌 默认为true
		'TOKEN_RESET' => true,
		/**
		 * 用户相关设置
		 */
		/**
		 * 最大缓存用户数
		 */
		'USER_MAX_CACHE' => 1000,
		/**
		 * 默认false 表示URL区分大小写 true则表示不区分大小写
		 */
		'URL_CASE_INSENSITIVE' => false,
		/**
		 * URL访问模式,可选参数0、1、2、3,代表以下四种模式：
		 * 0 (普通模式); 1 (PATHINFO 模式);
		 * 2 (REWRITE 模式); 3 (兼容模式)
		 * 默认为PATHINFO 模式
		 */
		'URL_MODEL' => 2,
		/**
		 * URL伪静态后缀设置
		 */
		'URL_HTML_SUFFIX' => 'shtml',
		/**
		 * URL禁止访问的后缀设置
		 */
		'URL_DENY_SUFFIX' => 'ico|png|gif|jpg',
		/**
		 * 默认模块
		 */
		'DEFAULT_MODULE' => 'Home',
		/**
		 * 系统数据加密设置
		 * 默认数据加密KEY
		 */
		'DATA_AUTH_KEY' => 'A$@4SM/{3dq,tcoQ@y]|l2=DK-#gZD:M(;PY+~JG' ,

);
$newconfig = array_merge ( $db, $config );
return array_merge ( $newconfig, $version );
