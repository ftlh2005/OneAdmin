<?php
// +----------------------------------------------------------------------
// | OneDream 后台菜单控制器
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2014 http://www.coolhots.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: CoolHots <coolhots@outlook.com>
// +----------------------------------------------------------------------
// | Date: 2014-4-5
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Admin\Controller\AdminController;

class MenuController extends AdminController {

	public function index($value='')
	{
		echo "string";
	}

	public function getMenu () {
		$userdata = session ( 'user_auth' );
		// roleid 为1是系统管理员，显示所有菜单
		if ($userdata ['roleid'] == 1) {
		}
		else {//其他角色根据权限查询菜单列表
			$ids=implode(',',session('roleauth'));
			if(!$ids) $this->display();//什么权限都没有的情况。
			$map['id']  = array('in',$ids);
			$map['pid'] = 0;
			$map['_logic'] = 'OR';
		}
		$list=D('Auth')->where($map)->order('sort')->select();
		//print_r($list);
		$listData = array();
		foreach ((array) $list as $key => $value) {
			//重组
			$listData[$key]['id'] = $value['id'];
			$listData[$key]['pid'] = $value['pid'];
			$listData[$key]['name'] = $value['title'];
			$listData[$key]['order'] = $value['sort'];
			$listData[$key]['url'] = U($value['url']);
		}
		$listData = list_to_tree($listData,'id','pid','menu');//转换成Tree

		$data = array();
		$data['status'] = 'y';
		$data['info']= $listData;
		$this->ajaxReturn($data);
	}

	public function cache () {
		if (IS_POST) {
			$key = I('post.data');
            if(empty($key)){
                $this->error('没有获取到清除动作！');
            }
            if(D('Manage')->delCache($key)){
                $this->success('缓存清空成功！');
            }else{
                $this->error('缓存清空失败！');
            }
		}
		$this->assign('list',D('Manage')->getCacheList());
		$this->display();
	}

}