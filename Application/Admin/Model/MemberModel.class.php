<?php
// +----------------------------------------------------------------------
// | OneDream 用户模型
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2014 http://www.coolhots.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: CoolHots <coolhots@outlook.com>
// +----------------------------------------------------------------------
// | Date: 2014-4-6
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model;

class MemberModel extends Model {

	//自动验证
	protected $_validate = array (
		array('username','require','用户名不能为空'), //用户名不能为空
		array('username','format_name_nocn','用户名由数字、26个英文字母或者下划线组成',0,'function'), //名称正则检查格式
		array('username','','用户名已经存在',0,'unique',1), // 在新增的时候验证name字段是否唯一
		array('username','1,30','用户名长度不合法',self::EXISTS_VALIDATE,'length'), //用户名长度

		/* 验证邮箱 */
		array('email','email','邮箱格式错误',2,'function',3), // 邮箱格式验证
		array('email', '1,32', '邮箱长度不合法', self::EXISTS_VALIDATE, 'length'), //邮箱长度不合法
		array('email', '', '邮箱被占用', self::EXISTS_VALIDATE, 'unique'), //邮箱被占用

		array('mobilephone','is_format_mobilephone','手机格式错误',2,'function',3), //新增或者编辑的时候不为空验证格式
		array('mobilephone','','手机已被使用',0,'unique',1), // 在新增的时候验证手机字段是否唯一

		array('password','require','请输入密码'), // 在新增的时候验证密码不能为空
		array('pwdconfirm','password','二次密码输入不一致',0,'confirm',1), // 验证密码是否一致
		array('roleid','require','请指定角色',0), // 在新增的时候验证角色ID不能为空
		array('roleid','isrole','用户所属角色不存在',0,'callback',3), //检测角色是否存在
		array('qq','is_format_qq','QQ号格式错误',2,'function',3), //新增或者编辑的时候不为空验证格式
		array('phone','is_format_phone','电话号码格式错误，由区号开头,如：028-XXX',2,'function',3), //新增或者编辑的时候不为空验证格式

	);

	//自动完成
	protected $_auto = array (
		array('updatetime','get_correction_time',3,'function'), // 对updatetime字段在新增和更新的时候写入当前时间戳 更新时间
		array('reg_ip', 'get_client_ip', self::MODEL_INSERT, 'function', 1), //注册时获取IP
		array('reg_time', NOW_TIME, self::MODEL_INSERT), // 对createtime字段在新增的时候写入当前时间戳 注册/创建时间
		array('remark','get_safe_html',3,'function'), // 对remark字段在新增会更新的时候进行安全过滤 备注
		array('remark','',2,'ignore'), // 对remark字段修改的时候如果留空忽略 备注
	);

	



	function lists($status = 1, $order = 'uid DESC', $field = true) {
		$map = array (
				'status' => $status 
		);
		return $this->field ( $field )->where ( $map )->order ( $order )->select ();
	}
	
	/**
	 * 根据用户编号获取用户所属的角色
	 */
	public function getUserRoles($uid) {
		$map ['uid'] = $uid;
		$role = $this->where ( $map )->find ();
		if ($role) {
		} else {
			return null;
		}
	}
	/**
	 * 用户登录
	 * 用户名
	 * 密码
	 */
	public function login($username, $password) {
		$map ['username'] = $username;
		$user = $this->where ( $map )->find ();
		if (! $user || 1 != $user ['status']) {
			$this->error = '用户不存在或已被禁用！'; // 应用级别禁用
		} else {
			$checkpassword = get_password ( $password, $user ['encrypt'] );
			if ($checkpassword == $user ['password']) {
				$data ['uid'] = $user ['uid'];
				$data ['login'] = $user ['login'] + 1;
				$data ['last_login_ip'] = get_client_ip(1);
				$data ['last_login_time'] = NOW_TIME;
				$this->save ( $data );
				/* 记录登录SESSION和COOKIES */
				$auth = array (
						'uid' => $user ['uid'],
						'nickname' => $user ['nickname'],
						'username' => $user ['nickname'],
						'last_login_time' => $user ['last_login_time'],
						'roleid' => $user ['roleid'],
						'auth' => $user ['auth'] 
				);
				
				session ( 'user_auth', $auth );
				session ( 'user_auth_sign', data_auth_sign ( $auth ) );
				$this->loadAuth($user ['roleid'],$user ['auth']);
				return $user ['uid'];
			} else {
				$this->error = '密码错误！';
			}
		}
	}
	
	/**
     * 根据用户的角色编号和用户单独分配的权限来读取规则菜单
	 */
	public function loadAuth($roleid,$userauth){
		//获取角色信息
		$role_info=D('Role')->field('auth,status')->where('id='.$roleid)->find();
		if($role_info['auth'] && $userauth){ //合并用户组权限和用户单独权限
			$auth = $role_info['auth'].','.$userauth;
		}else{
			$auth = $userauth?$userauth:$role_info['auth'];
		}
		$auth_array=explode(',',$auth);
		$auth_array=array_unique($auth_array);//去重复
		session ( 'roleauth', $auth_array );
		
	}
	
	/**
	 * 注销当前用户
	 *
	 * @return void
	 */
	public function logout() {
		session ( 'user_auth', null );
		session ( 'user_auth_sign', null );
		session('roleauth',null);
	}

	//根据用户UID取用户昵称
	public function getNickName($uid) {
		return $this->where ( array (
				'uid' => ( int ) $uid 
		) )->getField ( 'nickname' );
	}

	//验证角色是否存在
	public function isrole($roleid = ''){
		if($roleid == C('ADMIN_ROLE_ID') || $roleid == C('VISITORS_ROLE_ID')) return false;
		// $role = new \Common\Model\RoleModel;
		$role = M('Role');
		return $role->where('id='.$roleid)->find() ? true : false;
	}

}